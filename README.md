# daily

Scheduled build of Redox (and more, if you all want) on a Packet bare-metal host.

## Development 

Needs Go 1.13 or higher. Clone this project and

```
go build
```

We initialized this project with

```
go mod init gitlab.redox-os.org/coleman/daily 
```

This project is written in Go because there isn't a client library in Rust. 

## Explore the Packet API

Create an account, and [download the CLI](https://github.com/packethost/packet-cli/releases).
And set `PACKET_TOKEN` in your shell.

Retrieve a list of plans (Packet's term for device types). [See also this](https://www.packet.com/cloud/servers/).

```
packet plan get 
```
