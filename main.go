package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	mathRand "math/rand"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/SophisticaSean/easyssh"
	petname "github.com/dustinkirkland/golang-petname"
	"github.com/packethost/packngo"
	"golang.org/x/crypto/ssh"
)

var (
	githubDeployKey = flag.String("github-deploy-key", "$HOME/.ssh/id_rsa", "path to github deploy key")
	createServer    = flag.Bool("create-server", true, "whether to launch a new build-doer server")
	existingDevice  = flag.String("existing-device", "", "existing device name; if provided, prevents launch")
	autoDestroy     = flag.Bool("auto-destroy", false, "whether to tear down the server automatically after provisioning; good for CI")
)

func main() {
	flag.Parse()

	// expects PACKET_AUTH_TOKEN, not PACKET_TOKEN
	c, err := packngo.NewClient()
	if err != nil {
		log.Fatal(err)
	}

	projectID, err := GetProjectIDByName(c, "buildy")
	if err != nil {
		log.Fatal(err)
	}

	var d *packngo.Device

	if *existingDevice == "" {
		err := MakeSSHKeyPair("pub", "priv")
		if err != nil {
			log.Fatal(err)
		}
		cloudInitConfig, err := NewCloudInitConfig("pub")
		if err != nil {
			log.Fatal(err)
		}
		dcr := packngo.DeviceCreateRequest{}
		dcr.Hostname = NewServerName()
		dcr.Tags = []string{"build-doer"}
		dcr.ProjectID = projectID
		dcr.Plan = "baremetal_1"
		dcr.Facility = []string{"ewr1"}
		dcr.OS = "ubuntu_18_04"
		dcr.BillingCycle = "hourly"
		dcr.UserData = cloudInitConfig
		d, err = LaunchNewServer(c, &dcr, cloudInitConfig)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		d, err = GetDeviceIDByName(c, projectID, *existingDevice)
		if err != nil {
			log.Fatal(err)
		}

	}

	conf := easyssh.SSHConfig{}
	conf.User = "coleman"
	conf.Key = "priv"
	conf.Server = d.Network[0].Address
	fmt.Println("address", conf.Server)
	//scpFile(&conf, os.ExpandEnv(*githubDeployKey), "/home/coleman/.ssh/id_rsa")
	// prevent y/n prompt when updating libraries
	runCommand(&conf, "echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections", 20)
	runCommand(&conf, "sudo apt-get update", 300)
	runCommand(&conf, "sudo apt-get install git curl gcc open-iscsi multipath-tools autoconf autopoint bison build-essential cmake file flex fuse genisoimage gperf libc6-dev-i386 libfuse-dev libhtml-parser-perl libpng-dev libtool m4 nasm pkg-config syslinux-utils texinfo qemu-system-x86 qemu-kvm -y", 600)
	runCommand(&conf, "curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain \"nightly\"", 600)
	runCommand(&conf, "ssh-keyscan github.com >> ~/.ssh/known_hosts", 20)
	runCommand(&conf, "source $HOME/.cargo/env && cargo install xargo", 300)
	runCommand(&conf, "git clone https://gitlab.redox-os.org/redox-os/redox.git --origin upstream --recursive", 900)
	runCommand(&conf, "source $HOME/.cargo/env && cd redox && ./bootstrap.sh -d", 600)
	runCommand(&conf, "source $HOME/.cargo/env && cd redox && make all", 3600)

	if *autoDestroy {
		// tear down our server so we don't waste money
		resp, err := c.Devices.Delete(d.ID)
		if err != nil {
			log.Fatalf("device delete: %v", err)
		}
		log.Println("RESPONSE:")
		fmt.Println(resp.StatusCode)
		return

	}
	fmt.Printf("ssh -i priv coleman@%s\n", conf.Server)
}

// LaunchNewServer ...
func LaunchNewServer(c *packngo.Client, dcr *packngo.DeviceCreateRequest, cloudInitConfig string) (*packngo.Device, error) {

	dev, _, err := c.Devices.Create(dcr)
	if err != nil {
		log.Fatal(err)
	}
	ticker := time.NewTicker(10 * time.Second)
	devID := dev.ID

	var provisioned bool

	for {
		select {
		case <-ticker.C:
			d, _, err := c.Devices.Get(devID, nil)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("device state: %s\n", d.State)
			if strings.TrimSpace(d.State) == "active" {
				time.Sleep(5 * time.Second)
				provisioned = true
			}
		}
		if provisioned {
			break
		}
	}
	d, _, err := c.Devices.Get(devID, nil)
	return d, err
}

func runCommand(conf *easyssh.SSHConfig, cmd string, timeoutSec int) error {
	stdout, stderr, done, err := conf.Stream(cmd, timeoutSec)
	if err != nil {
		return err
	}
	var finished bool
	for {
		if finished {
			break
		}
		select {
		case out := <-stdout:
			fmt.Println(out)
		case out := <-stderr:
			// if timeout is reached, a timeout message is passed over stderr
			fmt.Println(out)
		case <-done:
			finished = true
			break
		}
	}
	return nil
}

func scpFile(conf *easyssh.SSHConfig, local, remote string) {
	err := conf.Scp(local, remote)
	if err != nil {
		panic(err)
	}
}

// NewServerName returns a random pet name for our server, which is a pet,
// albeit not a long-lived one. Sorry, little fella :(
func NewServerName() string {
	mathRand.Seed(time.Now().UTC().UnixNano())
	return petname.Generate(2, "-")
}

// GetProjectIDByName ...
func GetProjectIDByName(c *packngo.Client, name string) (string, error) {
	opts := packngo.ListOptions{}
	projects, _, err := c.Projects.List(&opts)
	if err != nil {
		return "", err
	}
	for _, p := range projects {
		if p.Name == name {
			return p.ID, nil
		}
	}
	return "", errors.New("project not found")

}

// GetDeviceIDByName ...
func GetDeviceIDByName(c *packngo.Client, projectID string, name string) (*packngo.Device, error) {

	devices, _, err := c.Devices.List(projectID, nil)
	if err != nil {
		return nil, err
	}

	for _, dev := range devices {
		if dev.Hostname == name {
			return &dev, nil
		}
	}

	return nil, errors.New("device name not found")
}

// NewCloudInitConfig templates a cloud-init config that we pass to the packet API
// upon launching our server. We make a user and some authorized ssh keys so we
// can log in to complete provisioning (or inspect our build).
func NewCloudInitConfig(pubKey string) (string, error) {
	data, err := ioutil.ReadFile(pubKey)
	if err != nil {
		return "", err
	}
	tmpl := `#cloud-config
users:
  - name: coleman
    groups: 
      - sudo 
      - kvm
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh-authorized-keys:
      - {{ .Key }}
`
	t, err := template.New("cloud-init").Parse(tmpl)
	if err != nil {
		return "", err
	}
	type Data struct {
		// ssh public key
		Key string
		// base64 encoded files
		RunScript string
		DeployKey string
		SSHConfig string
	}
	buf := bytes.NewBuffer([]byte(""))
	err = t.Execute(buf, Data{Key: strings.Replace(string(data), "\n", "", -1)})
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}

// MakeSSHKeyPair ...
func MakeSSHKeyPair(pubKeyPath, privateKeyPath string) error {
	privateKey, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return err
	}

	// generate and write private key as PEM
	privateKeyFile, err := os.Create(privateKeyPath)
	defer privateKeyFile.Close()
	if err != nil {
		return err
	}
	privateKeyPEM := &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privateKey)}
	// Actually write the private key file
	if err := pem.Encode(privateKeyFile, privateKeyPEM); err != nil {
		return err
	}

	err = os.Chmod(privateKeyPath, 0600)
	if err != nil {
		return err
	}

	// generate and write public key
	pub, err := ssh.NewPublicKey(&privateKey.PublicKey)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(pubKeyPath, ssh.MarshalAuthorizedKey(pub), 0655)
}
