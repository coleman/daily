module gitlab.redox-os.org/coleman/daily

require (
	github.com/SophisticaSean/easyssh v1.0.3
	github.com/dustinkirkland/golang-petname v0.0.0-20191129215211-8e5a1ed0cff0
	github.com/packethost/packngo v0.2.0
	golang.org/x/crypto v0.0.0-20200210222208-86ce3cb69678
)

go 1.13
